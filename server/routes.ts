const nextRoutes = require('next-routes')

const routes = (module.exports = nextRoutes())

routes.add('products', '/')

export default routes
