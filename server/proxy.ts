require('dotenv').config()

const devProxy: { [key: string]: {} } = {
    '/proxy': {
        target: process.env.API_URL,
        pathRewrite: { '^/proxy': '' },
        changeOrigin: true,
    },
}

export default devProxy
