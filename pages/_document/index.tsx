import * as React from 'react'
import Document, { Html, Head, Main, NextScript, DocumentContext } from 'next/document'
import { JssProvider, SheetsRegistry } from 'react-jss'

class WebAppDocument extends Document {
    static async getInitialProps({ renderPage }: DocumentContext) {
        /**
         * Decorate first render with SheetsRegistry and then put generated CSS into output
         */
        const sheets = new SheetsRegistry()

        const decoratePage = (Page: any) => (props: any) => (
            <JssProvider registry={sheets}>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <Page {...props} />
            </JssProvider>
        )

        const renderedPage = renderPage(decoratePage)

        // eslint-disable-next-line react/no-danger
        const styles = <style type="text/css" id="jss-ssr" dangerouslySetInnerHTML={{ __html: sheets.toString() }} />

        const props = {
            ...renderedPage,
            styles,
        }
        return props
    }

    render() {
        return (
            <Html>
                <Head />
                <body style={{ margin: 0, fontFamily: 'Verdana,Arial,Sans-Serif' }}>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default WebAppDocument
