import * as React from 'react'
import { NextPage } from 'next'

import { useTranslation } from 'react-i18next'
import { ProductsPageDataProvider } from 'src/components/ProductsPage/ProductsPageDataProvider'
import { createUseStyles } from 'react-jss'
import { useRemoveServerSideCss } from 'src/hooks/useRemoveServerSideCss'
import { ProductsCarousel } from 'src/components/ProductsPage/ProductsCarousel'
import { ProductCategoriesPanel } from '../../src/components/ProductsPage/ProductCategoriesPanel'
import { PageBorders } from '../../src/components/common/PageBorders/PageBorders'
import { ProductsGrid } from '../../src/components/ProductsPage/ProductsGrid'

interface Props {}
interface InitialProps {}

const useStyles = createUseStyles({
    heading: {
        color: '#0844a4',
        margin: 0,
        padding: '22px 0 36px 27px',
        fontSize: 24,
        fontWeight: 'normal',
    },
    carousel: {
        margin: '35px 0 46px',
    },
})

/**
 * Main products page
 */
const ProductsPage: NextPage<Props, InitialProps> = () => {
    useRemoveServerSideCss()
    const { t } = useTranslation('products')
    const styles = useStyles()

    return (
        <PageBorders>
            <ProductsPageDataProvider>
                <h1 className={styles.heading}>{t('Notebooks')}</h1>
                <ProductCategoriesPanel />
                <ProductsCarousel className={styles.carousel} />
                <ProductsGrid />
            </ProductsPageDataProvider>
        </PageBorders>
    )
}

ProductsPage.getInitialProps = async () => ({ namespacesRequired: ['common', 'salesItems'] })

export default ProductsPage
