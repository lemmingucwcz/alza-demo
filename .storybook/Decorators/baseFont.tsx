import * as React from 'react'

export const withBaseFont = () => (story: any) => (
    <div style={{ fontFamily: 'Verdana,Arial,Sans-Serif' }}>{story()}</div>
)
