# Alza Demo

Shop page demo created for Alza.cz

## How to run

### Demo deployed in Azure cloud

- No longer working since trial expired :-( 

### Docker image

You don't need to checkout project, just have Docker installed and:

- `docker pull lemmingucwcz/alzademo:latest`
- `docker run -d -p 3000:3000 lemmingucwcz/alzademo:latest`
- Open http://localhost:3000

### Local production mode

- `yarn`
- `yarn build`
- `yarn start`
- Open http://localhost:3000

### Local dev mode

- `yarn`
- `yarn dev`
- Open http://localhost:3000

