/**
 * Data about one product
 */
export interface ProductMo {
    /**
     * Product id
     */
    readonly id: number
    /**
     * Product name
     */
    readonly name: string
    /**
     * Product description
     */
    readonly spec: string
    /**
     * Image URL
     */
    readonly img: string
    /**
     * Formatted price with VAT
     */
    readonly price: string
    /**
     * Formatted price without VAT
     */
    readonly priceWithoutVat: string
    /**
     * Rating (stars)
     */
    readonly rating: number
    /**
     * Availability string
     */
    readonly avail: string
    /**
     * Price as a number
     */
    readonly priceNoCurrency: number
    /**
     * Product promos
     */
    readonly promos: PromoMo[]
}

/**
 * Data about promo
 */
export interface PromoMo {
    /**
     * Promo id
     */
    readonly id: number
    /**
     * Promo name
     */
    readonly name: string
}

/**
 * Data about products
 */
export interface ProductsDataMo {
    /**
     * Products in order for "top selling" in carousel
     */
    readonly topSellingProducts: ProductMo[]

    /**
     * Products in order for "top" in grid
     */
    readonly topProductsGrid: ProductMo[][]

    /**
     * Products in order for "top selling" in grid
     */
    readonly topSellingProductsGrid: ProductMo[][]

    /**
     * Products in order for "lowest price first" in grid
     */
    readonly lowestPriceFirstProductsGrid: ProductMo[][]

    /**
     * Products in order for "highest price first" in grid
     */
    readonly highestPriceFirstProductsGrid: ProductMo[][]
}
