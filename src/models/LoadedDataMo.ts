/**
 * Data loaded for api including loading / error state
 */
export interface LoadedDataMo<T> {
    /**
     * Data are loading right now
     */
    readonly loading: boolean
    /**
     * There was error loading data
     */
    readonly error: boolean
    /**
     * Loaded data
     */
    readonly data?: T
}
