/**
 * Product category
 */
export interface ProductCategoryMo {
    /**
     * Category id
     */
    readonly id: number
    /**
     * Category name
     */
    readonly name: string
}
