import * as React from 'react'
import { ProductMo } from 'src/models/ProductsData'
import { ProductCardCarousel } from '../ProductCardCarousel'
import { ProductCardFull } from '../ProductCardFull'

export default { title: 'Product panel' }

const product: ProductMo = {
    id: 5278177,
    avail: 'Skladem > 10 ks',
    img: 'https://cdn.alza.cz/Foto/f8/IM/IMMX18005.jpg',
    name: 'Immax Neo GU10 4,8W teplá bílá, stmívatelná, Zigbee 3.0',
    price: '309 Kč',
    priceWithoutVat: '255 Kč',
    rating: 2.606,
    priceNoCurrency: 309,
    spec:
        'LED žárovka chytrá, 4,8 W, jeden kus v balení, patice GU10, světelný tok 350 lm, napájecí napětí 230 V, stmívatelná, vydává teplé světlo, minimální chromatičnost 2700K, maximální chromatičnost 3000K, ZigBee, fungování přes centrální jednotku, kompatibilní s Google Assistant, Amazon Alexa, energetický štítek A+',
    promos: [
        {
            id: 5673872,
            name: 'Cestovní adaptér Wontravel',
        },
    ],
}

const product2: ProductMo = {
    id: 5278177,
    avail: 'Skladem > 10 ks',
    img: 'https://cdn.alza.cz/Foto/f8/IM/IMMX18005.jpg',
    name: 'Immax Neo GU10 4,8W teplá bílá, stmívatelná, Zigbee 3.0',
    price: '309 Kč',
    priceWithoutVat: '255 Kč',
    rating: 4.306,
    priceNoCurrency: 309,
    spec: 'LED žárovka chytrá',
    promos: [
        {
            id: 5673872,
            name: 'Cestovní adaptér Wontravel',
        },
        {
            id: 5673873,
            name: 'Cestovní adaptér Travel',
        },
    ],
}

export const carouselLongDescription = () => (
    <div style={{ padding: '20px' }}>
        <ProductCardCarousel product={product} />
    </div>
)

export const carouselShortDescription = () => (
    <div style={{ padding: '20px' }}>
        <ProductCardCarousel product={product2} />
    </div>
)

export const fullLongDescription = () => (
    <div style={{ padding: '20px' }}>
        <ProductCardFull product={product} />
    </div>
)

export const fullShortDescription = () => (
    <div style={{ padding: '20px' }}>
        <ProductCardFull product={product2} dropUp />
    </div>
)
