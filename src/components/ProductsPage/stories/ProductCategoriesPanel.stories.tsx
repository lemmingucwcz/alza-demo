import * as React from 'react'
import { ProductCategoryMo } from 'src/models/ProductCategoryMo'
import { productCategoriesContext } from 'src/context/productCategoriesContext'
import { ProductCategoriesPanel } from '../ProductCategoriesPanel'
import { mockLoadedData } from '../../../../test/utils'

export default { title: 'Product categories panel' }

const categories: ProductCategoryMo[] = [
    {
        id: 1,
        name: 'Macbook',
    },
    {
        id: 2,
        name: 'Herní',
    },
    {
        id: 3,
        name: 'Kancelářské',
    },
    {
        id: 4,
        name: 'Profesionální',
    },
    {
        id: 5,
        name: 'Stylové',
    },
    {
        id: 6,
        name: 'Základní',
    },
    {
        id: 7,
        name: 'Dotykové',
    },
    {
        id: 8,
        name: 'Na splátky',
    },
    {
        id: 9,
        name: 'VR Ready',
    },
    {
        id: 10,
        name: 'IRIS Graphics',
    },
    {
        id: 11,
        name: 'Brašny, batohy',
    },
    {
        id: 12,
        name: 'Příslušenství',
    },
]

export const panel = () => (
    <productCategoriesContext.Provider value={mockLoadedData(categories)}>
        <ProductCategoriesPanel />
    </productCategoriesContext.Provider>
)

export const fullShortDescription = () => <div style={{ padding: '20px' }}>Hello worls</div>
