import * as React from 'react'
import { createUseStyles } from 'react-jss'
import { useTranslation } from 'react-i18next'
import { ProductMo } from '../../models/ProductsData'
import { productCommonStyle } from './productCommonStyle'
import { ProductRatingStars } from './ProductRatingStars'
import { Dropdown, DropdownMenuItem } from '../common/Dropdown/Dropdown'

interface Props {
    /**
     * Product to show
     */
    readonly product: ProductMo
    /**
     * Button drop down drops up
     */
    readonly dropUp?: boolean
}

const useStyles = createUseStyles({
    ...productCommonStyle,
    wrapper: {
        ...productCommonStyle.wrapper,
        height: 391,
    },
    description: {
        ...productCommonStyle.description,
        margin: '8px 0 13px',
    },
    stars: {
        position: 'absolute',
        left: 0,
        bottom: -4,
    },
    imageWrapper: {
        flexShrink: 0,
        position: 'relative',
    },
    pricesRow: {
        fontSize: 12,
        flexShrink: 0,
        display: 'flex',
        alignItems: 'flex-end',
        margin: '15px 0 12px',
        lineHeight: '120%',
    },
    prices: {
        flex: 1,
        marginRight: 4,
        whiteSpace: 'nowrap',
    },
    availability: {
        fontSize: 11,
        fontWeight: 'bold',
        textAlign: 'center',
        flexShrink: 0,
    },
    dropDown: {
        marginBottom: 1,
    },
})

const actionItems: DropdownMenuItem[] = [
    {
        id: 'buyNow',
        caption: { key: 'Action_BuyNow', ns: 'products' },
    },
    {
        id: 'compare',
        caption: { key: 'Action_Compare', ns: 'products' },
    },
    {
        id: 'watch',
        caption: { key: 'Action_Watch', ns: 'products' },
    },
    {
        id: 'addToList',
        caption: { key: 'Action_AddToList', ns: 'products' },
    },
]

/**
 * Full product card used in grid
 */
export const ProductCardFull = ({ product, dropUp = false }: Props) => {
    const styles = useStyles()
    const { t } = useTranslation('products')

    const handleClick = () => {}

    return (
        <div className={styles.wrapper}>
            <div className={styles.name}>{product.name}</div>
            <div className={styles.description}>
                {product.spec}
                <br />
                <br />
                {product.promos.map(promo => (
                    <div key={promo.id}>
                        <strong>{t('FreePromo')}</strong>
                        &nbsp;
                        {promo.name}
                    </div>
                ))}
            </div>
            <div className={styles.imageWrapper}>
                <img className={styles.image} src={product.img} alt={product.name} />
                <ProductRatingStars className={styles.stars} rating={product.rating} />
            </div>
            <div className={styles.pricesRow}>
                <div className={styles.prices}>
                    <div className={styles.priceWithoutVat}>{product.priceWithoutVat}</div>
                    {product.price}
                </div>
                <Dropdown
                    items={actionItems}
                    caption={{ key: 'Action_Buy', ns: 'products' }}
                    onButtonClicked={handleClick}
                    onMenuItemClicked={handleClick}
                    dropUp={dropUp}
                    className={styles.dropDown}
                />
            </div>
            <div className={styles.availability}>{product.avail}</div>
        </div>
    )
}
