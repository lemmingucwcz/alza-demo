import * as React from 'react'
import { useReducer, useEffect } from 'react'
import { productsContext } from 'src/context/productsContext'
import { productCategoriesContext } from 'src/context/productCategoriesContext'
import { getInitialLoadedData, LoadedDataReducer, loadedDataReducer } from '../../store/loadedData/reducer'
import { ProductsDataMo } from '../../models/ProductsData'
import { ProductCategoryMo } from '../../models/ProductCategoryMo'
import { ProductCategoryAdapter } from '../../api/adapters/ProductCategoryAdapter'
import { ProductsAdapter } from '../../api/adapters/ProductsAdapter'
import { LoadErrorPanel } from '../common/LoadErrorPanel/LoadErrorPanel'
import { LoadingPanel } from '../common/LoadingPanel/LoadingPanel'

// Store to variables so that we don't re-create them every render
const productsInitialState = getInitialLoadedData<ProductsDataMo>()
const productCategoriesInitialState = getInitialLoadedData<ProductCategoryMo[]>()

/**
 * Handles loading and providing data to contexts for Products page
 */
export const ProductsPageDataProvider: React.FC<{}> = ({ children }) => {
    const [productsState, productsStateReducer] = useReducer(
        loadedDataReducer as LoadedDataReducer<ProductsDataMo>,
        productsInitialState,
    )
    const [productCategoriesState, productCategoriesStateReducer] = useReducer(
        loadedDataReducer as LoadedDataReducer<ProductCategoryMo[]>,
        productCategoriesInitialState,
    )

    useEffect(() => {
        // Load data on first render
        ProductCategoryAdapter.loadProductCategories(productCategoriesStateReducer)
        ProductsAdapter.loadProducts(productsStateReducer)
    }, [])

    const hasError = productsState.error || productCategoriesState.error
    const isLoaded = productsState.data && productCategoriesState.data

    if (hasError) {
        return <LoadErrorPanel />
    }

    if (!isLoaded) {
        return <LoadingPanel />
    }

    return (
        <productsContext.Provider value={productsState}>
            <productCategoriesContext.Provider value={productCategoriesState}>
                {children}
            </productCategoriesContext.Provider>
        </productsContext.Provider>
    )
}
