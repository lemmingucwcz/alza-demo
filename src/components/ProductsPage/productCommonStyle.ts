/**
 * Common style for product cards
 */
export const productCommonStyle = {
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        width: 152,
    },
    name: {
        fontSize: 14,
    },
    image: {
        width: 152,
        height: 160,
    },
    priceWithoutVat: {
        fontSize: 14,
        color: '#629c44',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        flexShrink: 0,
    },
    description: {
        fontSize: 11,
        flex: 1,
        color: '#7a7a7a',
        lineHeight: '125%',
        overflow: 'hidden',
    },
}
