import * as React from 'react'
import { useContext } from 'react'
import { createUseStyles } from 'react-jss'
import { useTranslation } from 'react-i18next'
import { productsContext } from '../../context/productsContext'
import { ProductMo } from '../../models/ProductsData'
import { ProductCardCarousel } from './ProductCardCarousel'
import { Carousel } from '../common/Carousel/Carousel'

interface Props {
    /**
     * Class for the wrapper
     */
    readonly className?: string
}

const useStyles = createUseStyles({
    title: {
        color: '#3d8df9',
        fontSize: 16,
        margin: '0 0 9px 27px',
        fontWeight: 'normal',
    },
})

const renderCarouselItem = (item: ProductMo) => <ProductCardCarousel product={item} key={item.id} />

/**
 * Renders products carousel
 */
export const ProductsCarousel = ({ className }: Props) => {
    const products = useContext(productsContext).data?.topSellingProducts || ([] as ProductMo[])
    const styles = useStyles()
    const { t } = useTranslation('products')

    return (
        <div className={className}>
            <h2 className={styles.title}>{t('List_TopSelling')}</h2>
            <Carousel items={products} renderItem={renderCarouselItem} visibleItemsCount={5} />
        </div>
    )
}
