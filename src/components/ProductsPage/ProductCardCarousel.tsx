import * as React from 'react'
import { createUseStyles } from 'react-jss'
import { ProductMo } from '../../models/ProductsData'
import { productCommonStyle } from './productCommonStyle'
import { ProductRatingStars } from './ProductRatingStars'

interface Props {
    /**
     * Product to show
     */
    readonly product: ProductMo
}

const useStyles = createUseStyles({
    ...productCommonStyle,
    wrapper: {
        ...productCommonStyle.wrapper,
        height: 287,
    },
    name: {
        ...productCommonStyle.name,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        marginTop: 10,
    },
    description: {
        ...productCommonStyle.description,
        height: 40,
        margin: '2px 0 10px',
    },
    stars: {
        marginTop: -5,
        flexGrow: 0,
        flexShrink: 0,
    },
})

/**
 * Stripped down version of product card used in carousel
 */
export const ProductCardCarousel = ({ product }: Props) => {
    const styles = useStyles()

    return (
        <div className={styles.wrapper}>
            <img className={styles.image} src={product.img} alt={product.name} />
            <div className={styles.name}>{product.name}</div>
            <ProductRatingStars className={styles.stars} rating={product.rating} />
            <div className={styles.description}>{product.spec}</div>
            <div className={styles.priceWithoutVat}>{product.priceWithoutVat}</div>
        </div>
    )
}
