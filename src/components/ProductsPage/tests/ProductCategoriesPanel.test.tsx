import * as React from 'react'
import { render } from '@testing-library/react'
import { ProductCategoriesPanel } from '../ProductCategoriesPanel'
import { productCategoriesContext } from '../../../context/productCategoriesContext'
import { ProductCategoryMo } from '../../../models/ProductCategoryMo'
import { mockLoadedData } from '../../../../test/utils'

const categories: ProductCategoryMo[] = [
    {
        id: 1,
        name: 'Macbook',
    },
    {
        id: 2,
        name: 'Herní',
    },
    {
        id: 3,
        name: 'Kancelářské',
    },
    {
        id: 4,
        name: 'Profesionální',
    },
]

describe('ProductCategoriesPanel', () => {
    test('Render', () => {
        const root = render(
            <productCategoriesContext.Provider value={mockLoadedData(categories)}>
                <ProductCategoriesPanel />
            </productCategoriesContext.Provider>,
        )

        expect(root).toMatchSnapshot()
    })
})
