import * as React from 'react'

import { render } from '@testing-library/react'
import { ProductRatingStars } from '../ProductRatingStars'

describe('ProductRatingStars', () => {
    test('render 3', () => {
        const root = render(<ProductRatingStars rating={3.3} />)

        expect(root).toMatchSnapshot()
    })
})
