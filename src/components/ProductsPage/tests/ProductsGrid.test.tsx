import * as React from 'react'
import { render, act } from '@testing-library/react'
import { productsContext } from '../../../context/productsContext'
import { mockProductsData } from './mockData'
import { mockLoadedData } from '../../../../test/utils'
import { ProductsGrid } from '../ProductsGrid'
import { ProductMo } from '../../../models/ProductsData'
import * as ProductCardFullModule from '../ProductCardFull'
import { componentTestIds } from '../../componentTestIds'

const buildTestId = (id: number) => `mockProductCard${id}`

const MockProductCardFull = ({ product }: { product: ProductMo }) => <div data-testid={buildTestId(product.id)} />

describe('ProductsGrid', () => {
    // Don't render full card, just a placeholder telling us product id
    jest.spyOn(ProductCardFullModule, 'ProductCardFull').mockImplementation(MockProductCardFull)

    test('Initial render', () => {
        const root = render(
            <productsContext.Provider value={mockLoadedData(mockProductsData)}>
                <ProductsGrid />
            </productsContext.Provider>,
        )

        // Check all three products were rendered
        const item1 = root.queryByTestId(buildTestId(1))
        const item2 = root.queryByTestId(buildTestId(2))
        const item3 = root.queryByTestId(buildTestId(2))
        expect(item1).not.toBeNull()
        expect(item2).not.toBeNull()
        expect(item3).not.toBeNull()
    })

    test('Change to top selling', () => {
        const root = render(
            <productsContext.Provider value={mockLoadedData(mockProductsData)}>
                <ProductsGrid />
            </productsContext.Provider>,
        )

        // Find and click "top selling" tab
        const topSellingTab = root.queryByTestId(componentTestIds.tabsTab(1))
        expect(topSellingTab).not.toBeNull()
        act(() => topSellingTab?.click())

        // Check item from that tab is rendered
        const item4 = root.queryByTestId(buildTestId(4))
        expect(item4).not.toBeNull()
    })
})
