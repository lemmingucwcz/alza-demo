import * as React from 'react'
import { render } from '@testing-library/react'
import { useContext } from 'react'
import { ProductsPageDataProvider } from '../ProductsPageDataProvider'
import { ProductCategoryAdapter } from '../../../api/adapters/ProductCategoryAdapter'
import { ProductsAdapter } from '../../../api/adapters/ProductsAdapter'
import { componentTestIds } from '../../componentTestIds'
import { LoadedDataActions } from '../../../store/loadedData/actions'
import { productsContext } from '../../../context/productsContext'
import { productCategoriesContext } from '../../../context/productCategoriesContext'
import { ProductsDataMo } from '../../../models/ProductsData'

const dataHolderSuccessTestId = 'dataHolderSuccessTestId'
const DataHolder = () => {
    const products = useContext(productsContext)
    const productCategories = useContext(productCategoriesContext)

    if (products.data && productCategories.data) {
        return <div data-testid={dataHolderSuccessTestId} />
    }

    return <div />
}

describe('ProductPageDataProvider', () => {
    it('should start loading and show loader at the beginning', () => {
        const loadProductCategoriesSpy = jest.spyOn(ProductCategoryAdapter, 'loadProductCategories').mockResolvedValue()
        const loadProductsSpy = jest.spyOn(ProductsAdapter, 'loadProducts').mockResolvedValue()

        const root = render(<ProductsPageDataProvider />)

        expect(loadProductCategoriesSpy).toHaveBeenCalled()
        expect(loadProductsSpy).toHaveBeenCalled()
        expect(root.queryAllByTestId(componentTestIds.loadingMessage)).toHaveLength(1)
    })

    it('should show error when there is loading error', () => {
        const loadProductCategoriesSpy = jest
            .spyOn(ProductCategoryAdapter, 'loadProductCategories')
            .mockImplementation(async dispatch => {
                dispatch(LoadedDataActions.loadError())
            })
        const loadProductsSpy = jest.spyOn(ProductsAdapter, 'loadProducts').mockResolvedValue()

        const root = render(<ProductsPageDataProvider />)

        expect(loadProductCategoriesSpy).toHaveBeenCalled()
        expect(loadProductsSpy).toHaveBeenCalled()
        expect(root.queryAllByTestId(componentTestIds.loadErrorMessage)).toHaveLength(1)
    })

    it('should render  error when there is loading error', () => {
        const loadProductCategoriesSpy = jest
            .spyOn(ProductCategoryAdapter, 'loadProductCategories')
            .mockImplementation(async dispatch => {
                dispatch(LoadedDataActions.dataLoaded([]))
            })
        const loadProductsSpy = jest.spyOn(ProductsAdapter, 'loadProducts').mockImplementation(async dispatch => {
            dispatch(LoadedDataActions.dataLoaded({} as ProductsDataMo))
        })

        const root = render(
            <ProductsPageDataProvider>
                <DataHolder />
            </ProductsPageDataProvider>,
        )

        expect(loadProductCategoriesSpy).toHaveBeenCalled()
        expect(loadProductsSpy).toHaveBeenCalled()
        expect(root.queryAllByTestId(dataHolderSuccessTestId)).toHaveLength(1)
    })
})
