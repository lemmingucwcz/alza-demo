import * as React from 'react'
import { render } from '@testing-library/react'
import { ProductCardCarousel } from '../ProductCardCarousel'
import { mockProduct } from './mockData'

describe('ProductCardCarousel', () => {
    test('Render', () => {
        const root = render(<ProductCardCarousel product={mockProduct} />)

        expect(root).toMatchSnapshot()
    })
})
