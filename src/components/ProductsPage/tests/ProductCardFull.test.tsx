import * as React from 'react'
import { render } from '@testing-library/react'
import { mockProduct } from './mockData'
import { ProductCardFull } from '../ProductCardFull'

describe('ProductCardCarousel', () => {
    test('Render', () => {
        const root = render(<ProductCardFull product={mockProduct} />)

        expect(root).toMatchSnapshot()
    })
})
