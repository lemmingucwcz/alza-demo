import { ProductMo, ProductsDataMo } from '../../../models/ProductsData'

export const mockProduct: ProductMo = {
    id: 5278177,
    avail: 'Skladem > 10 ks',
    img: 'https://cdn.alza.cz/Foto/f8/IM/IMMX18005.jpg',
    name: 'Immax Neo GU10 4,8W teplá bílá, stmívatelná, Zigbee 3.0',
    price: '309 Kč',
    priceWithoutVat: '255 Kč',
    rating: 3.606,
    priceNoCurrency: 309,
    spec: 'LED žárovka chytrá',
    promos: [
        {
            id: 5673872,
            name: 'Cestovní adaptér Wontravel',
        },
    ],
}

export const mockProductsData: ProductsDataMo = {
    topProductsGrid: [
        [
            { ...mockProduct, id: 1 },
            { ...mockProduct, id: 2 },
        ],
        [{ ...mockProduct, id: 3 }],
    ],
    topSellingProductsGrid: [[{ ...mockProduct, id: 4 }]],
} as ProductsDataMo
