import * as React from 'react'
import { createUseStyles } from 'react-jss'
import { useContext, useState } from 'react'
import { ProductsDataMo } from '../../models/ProductsData'
import { TabDefinition, Tabs } from '../common/Tabs/Tabs'
import { productsContext } from '../../context/productsContext'
import { ProductCardFull } from './ProductCardFull'

const useStyles = createUseStyles({
    grid: {
        marginTop: 12,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: '18px 53px 21px 36px',
    },
})

type TabIds = keyof Omit<ProductsDataMo, 'topSellingProducts'>

const tabDefs: TabDefinition<TabIds>[] = [
    {
        tab: 'topProductsGrid',
        caption: { key: 'List_TOP', ns: 'products' },
    },
    {
        tab: 'topSellingProductsGrid',
        caption: { key: 'List_TopSelling', ns: 'products' },
    },
    {
        tab: 'lowestPriceFirstProductsGrid',
        caption: { key: 'List_LowestPrice', ns: 'products' },
    },
    {
        tab: 'highestPriceFirstProductsGrid',
        caption: { key: 'List_HighestPrice', ns: 'products' },
    },
]

/**
 * Grid showing products
 */
export const ProductsGrid = () => {
    const [selectedTab, setSelectedTab] = useState<TabIds>('topProductsGrid')
    const productData = useContext(productsContext).data
    const styles = useStyles()

    const productGrid = productData?.[selectedTab] || []

    return (
        <div>
            <Tabs tabs={tabDefs} selectedTab={selectedTab} onSelectTab={setSelectedTab} />
            <div className={styles.grid}>
                {productGrid.map((rowData, n) => {
                    const isLastRow = n === productGrid.length - 1
                    return (
                        <div className={styles.row} key={rowData[0].id}>
                            {rowData.map(product => (
                                <ProductCardFull key={product.id} product={product} dropUp={isLastRow} />
                            ))}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
