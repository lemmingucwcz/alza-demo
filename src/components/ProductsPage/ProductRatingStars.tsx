import * as React from 'react'
import { createUseStyles } from 'react-jss'

const classNames = require('classnames')

interface Props {
    /**
     * Rating to show
     */
    readonly rating: number
    /**
     * Class name to append to wrapper
     */
    readonly className?: string
}

const useStyles = createUseStyles({
    wrapper: {
        fontSize: 27,
        transform: 'scaleX(0.95) translateX(-5px)',
    },
    star: {
        display: 'inline-block',
    },
    yellowStar: {
        color: '#fec63d',
    },
    grayStar: {
        color: '#ccc',
    },
})

/**
 * Shows product rating
 */
export const ProductRatingStars = ({ rating, className }: Props) => {
    const styles = useStyles()

    return (
        <div className={classNames([styles.wrapper, className])}>
            {[1, 2, 3, 4, 5].map(starNumber => {
                const starActive = Math.round(rating) >= starNumber
                const starClass = classNames({
                    [styles.star]: true,
                    [styles.yellowStar]: starActive,
                    [styles.grayStar]: !starActive,
                })
                return (
                    <span key={starNumber} className={starClass}>
                        ★
                    </span>
                )
            })}
        </div>
    )
}
