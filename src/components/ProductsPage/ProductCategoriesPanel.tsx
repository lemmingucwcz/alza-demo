import * as React from 'react'
import { createUseStyles } from 'react-jss'
import { useContext } from 'react'
import { productCategoriesContext } from '../../context/productCategoriesContext'

const useStyles = createUseStyles({
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: '0 50px 0 27px',
    },
    category: {
        boxSizing: 'border-box',
        fontSize: 12,
        border: '1px solid #c0c0c0',
        background: '#e6e6e6',
        padding: '10px',
        width: 143,
        height: 38,
        margin: '0 9px 10px 0',
    },
})

/**
 * Renders product categories
 */
export const ProductCategoriesPanel = () => {
    const styles = useStyles()
    const categories = useContext(productCategoriesContext).data || []

    return (
        <div className={styles.wrapper}>
            {categories.map(category => (
                <div className={styles.category} key={category.id}>
                    {category.name}
                </div>
            ))}
        </div>
    )
}
