import * as React from 'react'

import { render, act } from '@testing-library/react'
import { Carousel } from './Carousel'
import { componentTestIds } from '../../componentTestIds'

const items = ['ITEM1', 'ITEM2', 'ITEM3']

const renderItem = (item: string, testId?: string) => (
    <div data-testid={testId} key={item}>
        {item}
    </div>
)

describe('Carousel', () => {
    test('Render base state', () => {
        const root = render(<Carousel items={items} renderItem={renderItem} visibleItemsCount={2} />)

        const itemsWrapper = root.queryByTestId(componentTestIds.carousel.items)
        expect(itemsWrapper).toBeDefined()

        expect(itemsWrapper?.children.length).toBe(2)
        expect(itemsWrapper?.children[0].getAttribute('data-testid')).toBe('carouselItem0')
        expect(itemsWrapper?.children[1].getAttribute('data-testid')).toBe('carouselItem1')
    })

    test('moving back', () => {
        const root = render(<Carousel items={items} renderItem={renderItem} visibleItemsCount={2} />)

        // Move back one step
        const leftButton = root.queryByTestId(componentTestIds.carousel.leftButton)
        expect(leftButton).toBeDefined()
        act(() => leftButton?.click())

        const itemsWrapper = root.queryByTestId(componentTestIds.carousel.items)
        expect(itemsWrapper).toBeDefined()

        // Check items
        expect(itemsWrapper?.children.length).toBe(2)
        expect(itemsWrapper?.children[0].getAttribute('data-testid')).toBe('carouselItem2')
        expect(itemsWrapper?.children[1].getAttribute('data-testid')).toBe('carouselItem0')
    })

    test('moving forward', () => {
        const root = render(<Carousel items={items} renderItem={renderItem} visibleItemsCount={2} />)

        // Move forward four steps
        const rightButton = root.queryByTestId(componentTestIds.carousel.rightButton)
        expect(rightButton).toBeDefined()

        act(() => rightButton?.click())
        act(() => rightButton?.click())
        act(() => rightButton?.click())
        act(() => rightButton?.click())

        const itemsWrapper = root.queryByTestId(componentTestIds.carousel.items)
        expect(itemsWrapper).toBeDefined()

        // Check items
        expect(itemsWrapper?.children.length).toBe(2)
        expect(itemsWrapper?.children[0].getAttribute('data-testid')).toBe('carouselItem1')
        expect(itemsWrapper?.children[1].getAttribute('data-testid')).toBe('carouselItem2')
    })
})
