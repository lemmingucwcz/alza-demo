import React from 'react'
import { Carousel } from './Carousel'

export default { title: 'Carousel' }

const renderItem = (item: string) => (
    <div style={{ display: 'block', background: 'yellow', height: '100px' }}>{item}</div>
)

export const carousel = () => {
    return (
        <div>
            <Carousel
                items={['ITEM1', 'ITEM2', 'ITEM3', 'ITEM4', 'ITEM5', 'ITEM6', 'ITEM7']}
                visibleItemsCount={4}
                renderItem={renderItem}
            />
        </div>
    )
}
