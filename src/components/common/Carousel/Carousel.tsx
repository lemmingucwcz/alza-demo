import * as React from 'react'

import { useState, useMemo } from 'react'
import { createUseStyles } from 'react-jss'
import { componentTestIds } from '../../componentTestIds'

interface Props<ItemType> {
    /**
     * Items to show
     */
    readonly items: ItemType[]
    /**
     * Callback to render item
     */
    readonly renderItem: (item: ItemType, testId?: string) => React.ReactNode
    /**
     * How many items should be visible at once
     */
    readonly visibleItemsCount: number
}

const useStyles = createUseStyles({
    wrapper: { display: 'flex', alignItems: 'center' },
    buttonWrapper: { width: 48, boxSizing: 'border-box', textAlign: 'center', padding: '0 0 8px 0' },
    button: {
        color: 'white',
        background: '#3d8af7',
        border: '1px solid #000',
        borderRadius: 5,
        height: 64,
        width: 23,
        transition: 'background 0.1s',
        '&:hover': {
            background: '#69a7ff',
        },
        '&:focus': {
            outline: 'none',
        },
    },
    items: {
        flex: 1,
        display: 'flex',
        justifyContent: 'space-between',
        alignContent: 'stretch',
    },
})

/**
 * Renders carousel showing items in indefinite loop
 */
export const Carousel = <ItemType,>({ items, renderItem, visibleItemsCount }: Props<ItemType>) => {
    const [offset, setOffset] = useState(0)
    const styles = useStyles()

    // Pre-render items and memoize them
    const renderedItems = useMemo(() => {
        const rendered: React.ReactNode[] = []

        let itemOffset = offset
        for (let n = 0; n < visibleItemsCount; n += 1) {
            rendered.push(renderItem(items[itemOffset], componentTestIds.carousel.item(itemOffset)))
            itemOffset += 1
            if (itemOffset === items.length) {
                itemOffset = 0
            }
        }
        return rendered
    }, [items, renderItem, visibleItemsCount, offset])

    const handleMoveLeft = () => setOffset(oldOffset => (oldOffset === 0 ? items.length - 1 : oldOffset - 1))

    const handleMoveRight = () => setOffset(oldOffset => (oldOffset === items.length - 1 ? 0 : oldOffset + 1))

    return (
        <div className={styles.wrapper}>
            <div className={styles.buttonWrapper}>
                <button
                    className={styles.button}
                    onClick={handleMoveLeft}
                    type="button"
                    data-testid={componentTestIds.carousel.leftButton}
                >
                    &lt;
                </button>
            </div>
            <div className={styles.items} data-testid={componentTestIds.carousel.items}>
                {renderedItems}
            </div>
            <div className={styles.buttonWrapper}>
                <button
                    className={styles.button}
                    onClick={handleMoveRight}
                    type="button"
                    data-testid={componentTestIds.carousel.rightButton}
                >
                    &gt;
                </button>
            </div>
        </div>
    )
}
