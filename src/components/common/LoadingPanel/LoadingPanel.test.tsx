import * as React from 'react'
import { render } from '@testing-library/react'
import { componentTestIds } from '../../componentTestIds'
import { LoadingPanel } from './LoadingPanel'

describe('LoadingPanel', () => {
    test('render', () => {
        const root = render(<LoadingPanel />)

        const div = root.queryAllByTestId(componentTestIds.loadingMessage)

        expect(div).toHaveLength(1)
    })
})
