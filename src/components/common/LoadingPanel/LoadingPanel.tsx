import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { componentTestIds } from 'src/components/componentTestIds'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    panel: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    img: {
        width: 320,
        height: 320,
    },
})

/**
 * Shows loading message
 */
export const LoadingPanel = () => {
    const { t } = useTranslation()
    const styles = useStyles()

    return (
        <div className={styles.panel} data-testid={componentTestIds.loadingMessage}>
            <img src="/images/alzakSmall.png" className={styles.img} alt={t('loading')} />
            <div>{t('loading')}</div>
        </div>
    )
}
