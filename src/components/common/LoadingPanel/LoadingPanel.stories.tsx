import * as React from 'react'
import { LoadingPanel } from './LoadingPanel'

export default { title: 'Loading panel' }

export const panel = () => <LoadingPanel />
