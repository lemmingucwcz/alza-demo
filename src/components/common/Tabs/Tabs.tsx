import * as React from 'react'
import { createUseStyles } from 'react-jss'
import { componentTestIds } from '../../componentTestIds'
import { useRenderStringOrTranslatable } from '../../../hooks/useRenderStringOrTranslatable'

const classNames = require('classnames')

export interface TabDefinition<KeyType> {
    /**
     * Tab value (key, id)
     */
    readonly tab: KeyType

    /**
     * Tab caption to show directly or key to translate
     */
    readonly caption: StringOrTranslatable
}

interface Props<KeyType> {
    /**
     * Tab that should be displayed as selected
     */
    readonly selectedTab: KeyType
    /**
     * Tabs definitions
     */
    readonly tabs: Array<TabDefinition<KeyType>>
    /**
     * Callback to call when tab is selected (clicked)
     *
     * @param tab Clicked tab
     */
    readonly onSelectTab: (tab: KeyType) => void
}

const useStyles = createUseStyles({
    wrapper: {
        display: 'flex',
        paddingLeft: 23,
        borderBottom: '1px solid black',
    },
    tab: {
        height: 28,
        width: 124,
        fontSize: 12,
        border: '1px solid black',
        borderBottom: 'none',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#e6e6e6',
        marginRight: '-1px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        transition: 'background 0.1s',
        '&:hover': {
            backgroundColor: '#f3f3f3',
        },
        '&:focus': {
            outline: 'none',
        },
        cursor: 'default',
    },
    active: {
        backgroundColor: '#fff',
        transition: 'background 0.1s',
        '&:hover': {
            backgroundColor: '#fff',
        },
    },
})

/**
 * Renders Tabs component
 */
export const Tabs = <KeyType extends string | number>({ selectedTab, tabs, onSelectTab }: Props<KeyType>) => {
    const styles = useStyles()
    const { t } = useRenderStringOrTranslatable()

    const handleSelect = (tab: KeyType) => () => onSelectTab(tab)

    return (
        <div className={styles.wrapper}>
            {tabs.map((tab, order) => (
                <div
                    className={classNames(styles.tab, { [styles.active]: tab.tab === selectedTab })}
                    key={tab.tab}
                    onClick={handleSelect(tab.tab)}
                    role="button"
                    tabIndex={0}
                    data-testid={componentTestIds.tabsTab(order)}
                >
                    {t(tab.caption)}
                </div>
            ))}
        </div>
    )
}
