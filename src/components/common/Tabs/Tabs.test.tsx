import * as React from 'react'
import { act, render } from '@testing-library/react'
import { TabDefinition, Tabs } from './Tabs'
import { componentTestIds } from '../../componentTestIds'

const tabDef: TabDefinition<string>[] = [
    {
        tab: 'first',
        caption: 'First',
    },
    {
        tab: 'second',
        caption: 'Second',
    },
    {
        tab: 'third',
        caption: 'Third',
    },
]

describe('Tabs', () => {
    test('Render and call back', () => {
        const selectedTab = 'second'
        const onSelectTab = jest.fn()

        const root = render(<Tabs tabs={tabDef} selectedTab={selectedTab} onSelectTab={onSelectTab} />)

        // Check tabs rendered
        expect(root.queryAllByTestId(componentTestIds.tabsTab(0))).toHaveLength(1)
        const secondTab = root.queryAllByTestId(componentTestIds.tabsTab(1))[0]
        expect(secondTab).toBeDefined()
        const thirdTab = root.queryAllByTestId(componentTestIds.tabsTab(2))[0]
        expect(thirdTab).toBeDefined()

        // Test second tab has two classes => it is selected
        expect(secondTab.className.indexOf(' ')).toBeGreaterThan(0)

        // Click on the third tab
        act(() => {
            thirdTab.click()
        })

        // Check it called callback properly
        expect(onSelectTab).toHaveBeenCalledWith('third')
    })
})
