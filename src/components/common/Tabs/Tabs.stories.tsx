import React, { useState } from 'react'
import { TabDefinition, Tabs } from './Tabs'

export default { title: 'Tabs' }

const tabDef: TabDefinition<string>[] = [
    {
        tab: 'first',
        caption: 'First',
    },
    {
        tab: 'second',
        caption: 'Second',
    },
    {
        tab: 'third',
        caption: 'Third',
    },
    {
        tab: 'fourth',
        caption: 'Fourth',
    },
]

export const stringTabs = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [selectedTab, setSelectedTab] = useState('first')

    return <Tabs tabs={tabDef} selectedTab={selectedTab} onSelectTab={setSelectedTab} />
}
