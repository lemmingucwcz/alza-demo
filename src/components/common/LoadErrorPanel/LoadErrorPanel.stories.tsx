import * as React from 'react'
import { LoadErrorPanel } from './LoadErrorPanel'

export default { title: 'Load error' }

export const panel = () => <LoadErrorPanel />
