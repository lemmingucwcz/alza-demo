import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { componentTestIds } from 'src/components/componentTestIds'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    panel: { width: '100%', height: '200px', color: 'red', fontSize: '2.5em', textAlign: 'center', paddingTop: '20vh' },
})

/**
 * Shows load error message
 */
export const LoadErrorPanel = () => {
    const { t } = useTranslation()
    const styles = useStyles()

    return (
        <div className={styles.panel} data-testid={componentTestIds.loadErrorMessage}>
            {t('loadError')}
        </div>
    )
}
