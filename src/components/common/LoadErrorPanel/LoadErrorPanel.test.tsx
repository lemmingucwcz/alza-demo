import * as React from 'react'
import { render } from '@testing-library/react'
import { LoadErrorPanel } from './LoadErrorPanel'
import { componentTestIds } from '../../componentTestIds'

describe('LoadErrorPanel', () => {
    test('render', () => {
        const root = render(<LoadErrorPanel />)

        const div = root.queryAllByTestId(componentTestIds.loadErrorMessage)

        expect(div).toHaveLength(1)
    })
})
