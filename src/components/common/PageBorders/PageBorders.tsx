import * as React from 'react'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    outer: {
        background: '#f2f2f2',
    },
    inner: {
        background: '#fff',
        width: 953,
        margin: '0 auto',
    },
})

/**
 * Draws borders around actually used page area
 */
export const PageBorders: React.FC<{}> = ({ children }) => {
    const style = useStyles()

    return (
        <div className={style.outer}>
            <div className={style.inner}>{children}</div>
        </div>
    )
}
