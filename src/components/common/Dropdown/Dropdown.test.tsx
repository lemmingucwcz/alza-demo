import * as React from 'react'
import { render, act } from '@testing-library/react'
import { Dropdown, DropdownMenuItem } from './Dropdown'
import { componentTestIds } from '../../componentTestIds'

const itemDef: DropdownMenuItem[] = [
    {
        id: '1',
        caption: 'Item1',
    },
    {
        id: '2',
        caption: 'Item2',
    },
]

describe('Dropdown test', () => {
    test('Render, open and click', () => {
        const mockClickButton = jest.fn()
        const mockClickMenuItem = jest.fn()

        const root = render(
            <Dropdown
                items={itemDef}
                caption="Test"
                onButtonClicked={mockClickButton}
                onMenuItemClicked={mockClickMenuItem}
            />,
        )

        const button = root.queryByTestId(componentTestIds.dropDown.button)
        const caret = root.queryByTestId(componentTestIds.dropDown.caret)

        expect(button).toBeDefined()
        expect(caret).toBeDefined()

        // Click on button, check callback
        act(() => button?.click())
        expect(mockClickButton).toHaveBeenCalled()

        // Open menu
        act(() => caret?.click())
        const menuItem1 = root.queryByTestId(componentTestIds.dropDown.menuItem(0))
        expect(menuItem1).toBeDefined()

        // Click and check callback
        act(() => menuItem1?.click())
        expect(mockClickMenuItem).toHaveBeenCalledWith('1')
    })
})
