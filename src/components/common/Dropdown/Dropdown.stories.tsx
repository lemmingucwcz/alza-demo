import React from 'react'
import { Dropdown, DropdownMenuItem } from './Dropdown'

export default { title: 'Dropdown' }

const itemDef: DropdownMenuItem[] = [
    {
        id: '1',
        caption: 'Koupit zrychleně',
    },
    {
        id: '2',
        caption: 'Porovnat',
    },
    {
        id: '3',
        caption: 'Hlídat',
    },
    {
        id: '4',
        caption: 'Přidat do seznamu',
    },
]

export const stringDropDown = () => {
    return (
        <div style={{ paddingLeft: 100 }}>
            <Dropdown caption="Koupit" items={itemDef} onButtonClicked={() => {}} onMenuItemClicked={() => {}} />
        </div>
    )
}

export const stringDropUp = () => {
    return (
        <div style={{ paddingLeft: 100, paddingTop: 150 }}>
            <Dropdown caption="Go Up!" items={itemDef} dropUp onButtonClicked={() => {}} onMenuItemClicked={() => {}} />
        </div>
    )
}
