import * as React from 'react'
import { createUseStyles } from 'react-jss'
import { useState, useEffect, MouseEvent } from 'react'
import { useRenderStringOrTranslatable } from '../../../hooks/useRenderStringOrTranslatable'
import { componentTestIds } from '../../componentTestIds'

const classNames = require('classnames')

export interface DropdownMenuItem {
    /**
     * Dropdown item key
     */
    readonly id: string
    /**
     * Name to show directly or key to translate
     */
    readonly caption: StringOrTranslatable
}

interface Props {
    /**
     * Items to show
     */
    readonly items: DropdownMenuItem[]
    /**
     * Main button caption or key to translate
     */
    readonly caption: StringOrTranslatable
    /**
     * Show menu up instead of down
     */
    readonly dropUp?: boolean
    /**
     * Class name to apply to wrapper
     */
    readonly className?: string
    /**
     * Called when main button is clicked
     */
    readonly onButtonClicked: () => void
    /**
     * Called when menu item is clicked
     *
     * @param id: Clicked menu item id
     */
    readonly onMenuItemClicked: (id: string) => void
}

const useStyles = createUseStyles<{ dropUp: boolean }>({
    wrapper: {
        display: 'inline-block',
        position: 'relative',
        fontSize: 1,
        whiteSpace: 'nowrap',
    },
    button: {
        background: '#fff',
        fontSize: 12,
        height: 22,
        border: '1px solid #000',
        borderRight: 'none',
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5,
        minWidth: 60,
        textAlign: 'left',
        transition: 'background 0.1s',
        '&:hover': {
            background: '#f3f3f3',
        },
        '&:focus': {
            outline: 'none',
        },
    },
    caret: {
        height: 22,
        width: 23,
        fontSize: 12,
        padding: 0,
        border: '1px solid #000',
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        background: '#e6e6e6',
        transition: 'background 0.1s',
        '&:hover': {
            background: '#f3f3f3',
        },
        '&:focus': {
            outline: 'none',
        },
    },
    caretIcon: {
        display: 'inline-block',
        width: 0,
        height: 0,
        verticalAlign: 'middle',
        borderTop: '4px dashed #444',
        borderRight: '4px solid transparent',
        borderLeft: '4px solid transparent',
    },
    menu: ({ dropUp }) => {
        const base = {
            position: 'absolute',
            right: 0,
            zIndex: 1000,
            listStyle: 'none',
            backgroundColor: '#fff',
            border: '1px solid #bbb',
            borderRadius: 5,
            boxShadow: '0 2px 6px rgba(0, 0, 0, 0.2)',
        }

        return dropUp ? { ...base, bottom: '100%' } : { ...base, top: '100%' }
    },
    menuItem: {
        display: 'block',
        width: '100%',
        border: 'none',
        background: 'transparent',
        whiteSpace: 'nowrap',
        textAlign: 'right',
        padding: '6px 8px',
        transition: 'background 0.1s',
        '&:hover': {
            background: '#f3f3f3',
        },
        '&:focus': {
            outline: 'none',
        },
    },
})

/**
 * Renders button with drop down (or up :-) ) options
 */
export const Dropdown = ({ items, caption, onButtonClicked, onMenuItemClicked, dropUp = false, className }: Props) => {
    const styles = useStyles({ dropUp })
    const [open, setOpen] = useState(false)
    const { t } = useRenderStringOrTranslatable()

    // Close menu when window is clicked
    useEffect(() => {
        const closeMenu = () => setOpen(false)
        window.addEventListener('mousedown', closeMenu)
        return () => window.removeEventListener('mousedown', closeMenu)
    }, [])

    const toggleOpen = () => setOpen(state => !state)

    // Prevent mousedown propagation so that we do not close menu when we don't want to
    const preventMouseDown = (event: MouseEvent) => {
        if (open) {
            event.stopPropagation()
        }
    }

    const handleItemClick = (id: string) => () => onMenuItemClicked(id)

    return (
        <div className={classNames([styles.wrapper, className])}>
            <button
                type="button"
                className={styles.button}
                onClick={onButtonClicked}
                data-testid={componentTestIds.dropDown.button}
            >
                {t(caption)}
            </button>
            <button
                type="button"
                className={styles.caret}
                onClick={toggleOpen}
                onMouseDown={preventMouseDown}
                data-testid={componentTestIds.dropDown.caret}
            >
                <span className={styles.caretIcon} />
            </button>

            {open && (
                <div className={styles.menu}>
                    {items.map((item, i) => (
                        <button
                            key={item.id}
                            type="button"
                            data-testid={componentTestIds.dropDown.menuItem(i)}
                            className={styles.menuItem}
                            onMouseDown={preventMouseDown}
                            onClick={handleItemClick(item.id)}
                        >
                            {t(item.caption)}
                        </button>
                    ))}
                </div>
            )}
        </div>
    )
}
