/**
 * Test IDs for components
 */
export const componentTestIds = {
    loadErrorMessage: 'loadErrorMessage',
    loadingMessage: 'loadingMessage',
    tabsTab: (n: number) => `tab${n}`,
    dropDown: {
        button: 'dropDownButton',
        caret: 'dropDownCaret',
        menuItem: (n: number) => `dropDownMenuItem${n}`,
    },
    carousel: {
        leftButton: 'carouselLeftButton',
        rightButton: 'carouselRightButton',
        items: 'carouselItems',
        item: (offset: number) => `carouselItem${offset}`,
    },
}
