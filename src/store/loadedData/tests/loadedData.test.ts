import { getInitialLoadedData, loadedDataReducer } from '../reducer'
import { LoadedDataActions } from '../actions'

/**
 * Tests reducer & actions
 */
describe('loadedData reducer & actions', () => {
    const initialData = getInitialLoadedData<number>()

    test('loading', () => {
        const result = loadedDataReducer(initialData, LoadedDataActions.startLoading())

        expect(result).toEqual({
            loading: true,
            error: false,
        })
    })

    test('load error', () => {
        const result = loadedDataReducer({ ...initialData, loading: true }, LoadedDataActions.loadError())

        expect(result).toEqual({
            loading: false,
            error: true,
        })
    })

    test('load success', () => {
        const result = loadedDataReducer({ ...initialData, loading: true }, LoadedDataActions.dataLoaded(123))

        expect(result).toEqual({
            loading: false,
            error: false,
            data: 123,
        })
    })

    test('unknown action', () => {
        const result = loadedDataReducer(initialData, { type: 'XXX' } as any)

        expect(result).toEqual(initialData)
    })
})
