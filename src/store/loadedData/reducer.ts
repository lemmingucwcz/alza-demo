import { LoadedDataMo } from 'src/models/LoadedDataMo'
import { Dispatch } from 'react'
import { LoadedDataAction } from './actions'

// We need to use factory function to get correct typing on return
export const getInitialLoadedData = <T>(): LoadedDataMo<T> => ({
    loading: false,
    error: false,
})

export type LoadedDataReducer<T> = (state: LoadedDataMo<T>, action: LoadedDataAction<T>) => LoadedDataMo<T>

/**
 * Reducer for loaded data
 */
export const loadedDataReducer = <T>(state: LoadedDataMo<T>, action: LoadedDataAction<T>): LoadedDataMo<T> => {
    switch (action.type) {
        case 'START_LOADING':
            return { ...state, error: false, loading: true }
        case 'LOAD_ERROR':
            return { ...state, error: true, loading: false }
        case 'DATA_LOADED':
            return { data: action.data, error: false, loading: false }
        default:
            return state
    }
}

export type LoadedDataDispatch<T> = Dispatch<LoadedDataAction<T>>
