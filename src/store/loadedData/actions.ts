export type LoadedDataAction<T> =
    | {
          type: 'START_LOADING'
      }
    | {
          type: 'LOAD_ERROR'
      }
    | {
          type: 'DATA_LOADED'
          data: T
      }

export const LoadedDataActions = {
    startLoading: <T>(): LoadedDataAction<T> => ({
        type: 'START_LOADING',
    }),

    loadError: <T>(): LoadedDataAction<T> => ({
        type: 'LOAD_ERROR',
    }),

    dataLoaded: <T>(data: T): LoadedDataAction<T> => ({
        type: 'DATA_LOADED',
        data,
    }),
}
