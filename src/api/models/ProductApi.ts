/**
 * Data about product from API
 */
export interface ProductApi {
    /**
     * Product id
     */
    readonly id?: number
    /**
     * Product name
     */
    readonly name?: string
    /**
     * Product description
     */
    readonly spec?: string
    /**
     * Image URL
     */
    readonly img?: string
    /**
     * Formatted price with VAT
     */
    readonly price?: string
    /**
     * Formatted price without VAT
     */
    readonly priceWithoutVat?: string
    /**
     * Rating (stars)
     */
    readonly rating?: number
    /**
     * Availability string
     */
    readonly avail?: string
    /**
     * Price as a number
     */
    readonly priceNoCurrency?: number
    /**
     * Product promos
     */
    readonly promos?: Array<PromoApi>
}

/**
 * Data about Promo from API
 */
export interface PromoApi {
    /**
     * Promo ID
     */
    readonly id?: number

    /**
     * Promo name
     */
    readonly name?: string
}

/**
 * Products response from APi
 */
export interface ProductResponseApi {
    readonly err?: number
    readonly msg?: string
    readonly data?: ProductApi[]
}
