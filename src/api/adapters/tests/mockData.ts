/**
 * Mock data for products
 */
import { ProductApi, PromoApi } from '../../models/ProductApi'

const goodPromo1: PromoApi = {
    id: 14,
    name: 'Hello',
}

const goodPromo2: PromoApi = {
    id: 17,
    name: 'World',
}

export const badPromo1: PromoApi = {
    id: 7,
    name: '',
}

const badPromo2: PromoApi = {
    name: 'Promo',
}

export const goodProductWithGoodPromos: ProductApi = {
    id: 3,
    priceNoCurrency: 123,
    promos: [goodPromo1, goodPromo2],
    spec: 'Some Spec',
    rating: 5,
    priceWithoutVat: '123 CZK',
    price: '100 CZK',
    name: 'Our product',
    img: 'http://123',
    avail: 'Now',
}

export const goodProductWithBadPromo1: ProductApi = {
    ...goodProductWithGoodPromos,
    promos: [badPromo1],
}

export const goodProductWithGoodAndBadPromo2: ProductApi = {
    ...goodProductWithGoodPromos,
    promos: [goodPromo1, badPromo2],
}

export const goodProductWithoutPromosAndSpec: ProductApi = {
    ...goodProductWithGoodPromos,
    spec: undefined,
    promos: undefined,
}

export const badProduct1Empty: ProductApi = {}

export const badProduct1RatingOutOfRange: ProductApi = {
    ...goodProductWithoutPromosAndSpec,
    rating: 5.5,
}
