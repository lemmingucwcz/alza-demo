import { LoggingService } from '../../services/LoggingService'
import { HttpService } from '../../services/HttpService'
import { ProductsAdapter } from '../ProductsAdapter'
import { LoadedDataActions } from '../../../store/loadedData/actions'
import { ProductApi, ProductResponseApi } from '../../models/ProductApi'
import { goodProductWithBadPromo1, goodProductWithoutPromosAndSpec } from './mockData'
import { ProductsDataMo } from '../../../models/ProductsData'

const buildResponse = (response: ProductResponseApi) =>
    (({
        json: () => Promise.resolve(response),
    } as unknown) as Response)

const productsToSort: ProductApi[] = [
    {
        ...goodProductWithoutPromosAndSpec,
        id: 6,
        priceNoCurrency: 1256,
    },
    {
        ...goodProductWithoutPromosAndSpec,
        id: 5,
        priceNoCurrency: 6,
    },
    {
        ...goodProductWithoutPromosAndSpec,
        id: 4,
        priceNoCurrency: 18,
    },
    {
        ...goodProductWithoutPromosAndSpec,
        id: 3,
        priceNoCurrency: 98,
    },
    {
        ...goodProductWithoutPromosAndSpec,
        id: 2,
        priceNoCurrency: 3,
    },
    {
        ...goodProductWithoutPromosAndSpec,
        id: 1,
        priceNoCurrency: 546,
    },
]

/**
 * Flatten 2-dimensional array into 1-dimensonal. Seems Array.prototype.flat() is not in my version of node.
 */
const flat = <T>(arr: T[][]): T[] =>
    arr.reduce((acc, element) => {
        acc.push(...element)
        return acc
    }, [] as T[])

describe('ProductsAdapter', () => {
    const mockDispatch = jest.fn()
    const logErrorSpy = jest.spyOn(LoggingService, 'logError')
    const logWarningSpy = jest.spyOn(LoggingService, 'logWarning')

    beforeEach(jest.resetAllMocks)

    test('Network error', async () => {
        jest.spyOn(HttpService, 'post').mockResolvedValue()

        await ProductsAdapter.loadProducts(mockDispatch)

        expect(mockDispatch).toHaveBeenNthCalledWith(1, LoadedDataActions.startLoading())
        expect(mockDispatch).toHaveBeenNthCalledWith(2, LoadedDataActions.loadError())
    })

    test('API error', async () => {
        jest.spyOn(HttpService, 'post').mockResolvedValue(
            buildResponse({
                err: 1,
                msg: 'Bad id',
            }),
        )

        await ProductsAdapter.loadProducts(mockDispatch)

        expect(mockDispatch).toHaveBeenNthCalledWith(1, LoadedDataActions.startLoading())
        expect(mockDispatch).toHaveBeenNthCalledWith(2, LoadedDataActions.loadError())
        expect(logErrorSpy).toHaveBeenCalled()
    })

    test('Loaded with errors', async () => {
        jest.spyOn(HttpService, 'post').mockResolvedValue(
            buildResponse({
                err: 0,
                data: [goodProductWithoutPromosAndSpec, goodProductWithBadPromo1],
            }),
        )

        await ProductsAdapter.loadProducts(mockDispatch)

        expect(mockDispatch).toHaveBeenCalledTimes(2)
        expect(mockDispatch).toHaveBeenNthCalledWith(1, LoadedDataActions.startLoading())
        expect(logWarningSpy).toHaveBeenCalled()
        const secondCall = mockDispatch.mock.calls[1][0]
        expect(secondCall.type).toBe('DATA_LOADED')
        expect(secondCall.data).toBeDefined()
        const data = secondCall.data as ProductsDataMo
        expect(data.topProductsGrid).toHaveLength(1)
        expect(data.topProductsGrid[0]).toHaveLength(1)
    })

    test('Loaded OK + sorting', async () => {
        jest.spyOn(HttpService, 'post').mockResolvedValue(
            buildResponse({
                err: 0,
                data: productsToSort,
            }),
        )

        await ProductsAdapter.loadProducts(mockDispatch)

        expect(mockDispatch).toHaveBeenCalledTimes(2)
        expect(mockDispatch).toHaveBeenNthCalledWith(1, LoadedDataActions.startLoading())
        expect(logWarningSpy).not.toHaveBeenCalled()

        const secondCall = mockDispatch.mock.calls[1][0]
        expect(secondCall.type).toBe('DATA_LOADED')
        expect(secondCall.data).toBeDefined()
        const data = secondCall.data as ProductsDataMo
        expect(data.topSellingProducts.map(({ id }) => id)).toEqual([1, 2, 3, 4, 5, 6])
        expect(data.topProductsGrid).toHaveLength(2)
        expect(data.topProductsGrid[0]).toHaveLength(5)
        expect(data.topProductsGrid[1]).toHaveLength(1)
        expect(flat(data.topProductsGrid).map(({ id }) => id)).toEqual([6, 5, 4, 3, 2, 1])
        expect(flat(data.topSellingProductsGrid).map(({ id }) => id)).toEqual([1, 2, 3, 4, 5, 6])
        expect(flat(data.lowestPriceFirstProductsGrid).map(({ priceNoCurrency }) => priceNoCurrency)).toEqual([
            3,
            6,
            18,
            98,
            546,
            1256,
        ])
        expect(flat(data.highestPriceFirstProductsGrid).map(({ priceNoCurrency }) => priceNoCurrency)).toEqual([
            1256,
            546,
            98,
            18,
            6,
            3,
        ])
    })
})
