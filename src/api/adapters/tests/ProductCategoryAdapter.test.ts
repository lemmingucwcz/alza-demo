import { ProductCategoryAdapter } from '../ProductCategoryAdapter'
import { LoadedDataActions } from '../../../store/loadedData/actions'

describe('ProductCategoryAdapter', () => {
    const mockDispatch = jest.fn()

    test('Mock load', async () => {
        await ProductCategoryAdapter.loadProductCategories(mockDispatch)

        expect(mockDispatch).toHaveBeenCalledTimes(2)
        expect(mockDispatch).toHaveBeenNthCalledWith(1, LoadedDataActions.startLoading())
        const secondCall = mockDispatch.mock.calls[1][0]
        expect(secondCall.type).toBe('DATA_LOADED')
        expect(secondCall.data).toBeDefined()
        expect(secondCall.data).toHaveLength(12)
    })
})
