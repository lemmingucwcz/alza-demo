import { ProductCategoryMo } from '../../models/ProductCategoryMo'
import { LoadedDataDispatch } from '../../store/loadedData/reducer'
import { LoadedDataActions } from '../../store/loadedData/actions'

const mockData: ProductCategoryMo[] = [
    {
        id: 1,
        name: 'Macbook',
    },
    {
        id: 2,
        name: 'Herní',
    },
    {
        id: 3,
        name: 'Kancelářské',
    },
    {
        id: 4,
        name: 'Profesionální',
    },
    {
        id: 5,
        name: 'Stylové',
    },
    {
        id: 6,
        name: 'Základní',
    },
    {
        id: 7,
        name: 'Dotykové',
    },
    {
        id: 8,
        name: 'Na splátky',
    },
    {
        id: 9,
        name: 'VR Ready',
    },
    {
        id: 10,
        name: 'IRIS Graphics',
    },
    {
        id: 11,
        name: 'Brašny, batohy',
    },
    {
        id: 12,
        name: 'Příslušenství',
    },
]

export const ProductCategoryAdapter = {
    /**
     * Loads products categories. Mock-wise.
     *
     * @param dispatch Dispatch used to set loading/error/data into state
     */
    loadProductCategories: async (dispatch: LoadedDataDispatch<ProductCategoryMo[]>) => {
        dispatch(LoadedDataActions.startLoading())
        dispatch(LoadedDataActions.dataLoaded(mockData))
    },
}
