import { ProductApi, PromoApi } from 'src/api/models/ProductApi'
import { ProductMo, PromoMo } from 'src/models/ProductsData'

const isNumber = (value: number | null | undefined): value is number => typeof value === 'number'
class MapError extends Error {}

const mapPromo = (promo: PromoApi): PromoMo => {
    const { id, name } = promo

    // Sanity check data from JSON
    if (isNumber(id) && name) {
        // Data OK, can map
        return {
            id,
            name,
        } as PromoMo
    }

    throw new MapError()
}

const mapProduct = (product: ProductApi): ProductMo => {
    const { id, avail, img, name, price, priceWithoutVat, rating, priceNoCurrency } = product

    // Sanity check data from JSON
    if (
        isNumber(id) &&
        avail &&
        img &&
        name &&
        price &&
        priceWithoutVat &&
        isNumber(priceNoCurrency) &&
        isNumber(rating) &&
        rating >= 0 &&
        rating <= 5
    ) {
        // Data OK, can map
        return {
            id,
            avail,
            img,
            name,
            price,
            priceWithoutVat,
            rating,
            priceNoCurrency,
            spec: product.spec || '',
            promos: (product.promos || []).map(mapPromo),
        } as ProductMo
    }

    throw new MapError()
}

export const ProductsMapper = {
    /**
     * Maps product(s) from API objects to our model objects and sanity checks them in the process
     *
     * @param products Products to map
     *
     * @return Object containing array of mapped products and number of products that could not be
     * mapped due to errors.
     */
    mapProductsFromApi: (products: ProductApi[]) =>
        products.reduce<{ products: ProductMo[]; numErrors: number }>(
            (acc, product) => {
                try {
                    acc.products.push(mapProduct(product))
                } catch (e) {
                    acc.numErrors += 1
                }
                return acc
            },
            { products: [], numErrors: 0 },
        ),
}
