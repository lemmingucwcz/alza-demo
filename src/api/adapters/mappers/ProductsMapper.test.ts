import { ProductsMapper } from './ProductsMapper'
import {
    badProduct1Empty,
    badProduct1RatingOutOfRange,
    goodProductWithBadPromo1,
    goodProductWithGoodAndBadPromo2,
    goodProductWithGoodPromos,
    goodProductWithoutPromosAndSpec,
} from '../tests/mockData'

const goodProductWithGoodPromosMapped = {
    avail: 'Now',
    id: 3,
    img: 'http://123',
    name: 'Our product',
    price: '100 CZK',
    priceNoCurrency: 123,
    priceWithoutVat: '123 CZK',
    promos: [
        {
            id: 14,
            name: 'Hello',
        },
        {
            id: 17,
            name: 'World',
        },
    ],
    rating: 5,
    spec: 'Some Spec',
}

const goodProductWithoutPromosAndSpecMapped = {
    avail: 'Now',
    id: 3,
    img: 'http://123',
    name: 'Our product',
    price: '100 CZK',
    priceNoCurrency: 123,
    priceWithoutVat: '123 CZK',
    promos: [],
    rating: 5,
    spec: '',
}

describe('Products mapper', () => {
    test('Good and bad product', () => {
        const result = ProductsMapper.mapProductsFromApi([goodProductWithGoodPromos, goodProductWithBadPromo1])

        expect(result).toEqual({
            numErrors: 1,
            products: [goodProductWithGoodPromosMapped],
        })
    })

    test('Minimal good product', () => {
        const result = ProductsMapper.mapProductsFromApi([goodProductWithoutPromosAndSpec])

        expect(result).toEqual({
            numErrors: 0,
            products: [goodProductWithoutPromosAndSpecMapped],
        })
    })

    test('Bad products', () => {
        const result = ProductsMapper.mapProductsFromApi([
            goodProductWithGoodAndBadPromo2,
            badProduct1Empty,
            badProduct1RatingOutOfRange,
        ])

        expect(result).toEqual({
            numErrors: 3,
            products: [],
        })
    })
})
