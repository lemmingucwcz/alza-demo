import { LoadedDataDispatch } from 'src/store/loadedData/reducer'
import { HttpService } from '../services/HttpService'
import { ProductMo, ProductsDataMo } from '../../models/ProductsData'
import { ProductResponseApi } from '../models/ProductApi'
import { LoggingService } from '../services/LoggingService'
import { ProductsMapper } from './mappers/ProductsMapper'
import { LoadedDataActions } from '../../store/loadedData/actions'

const PRODUCTS_PER_ROW = 5

const topSellingComparator = (a: ProductMo, b: ProductMo) => a.id - b.id
const lowestPriceComparator = (a: ProductMo, b: ProductMo) => a.priceNoCurrency - b.priceNoCurrency
const highestPriceComparator = (a: ProductMo, b: ProductMo) => b.priceNoCurrency - a.priceNoCurrency

const findRequest = {
    filterParameters: {
        id: 18855843,
        sInStockOnly: false,
        newsOnly: false,
        wearType: 0,
        orderBy: 0,
        page: 1,
        params: {
            tId: 0,
            v: [],
        },
        producers: [],
        sendPrices: true,
        type: 'action',
        typeId: '',
        branchId: '',
    },
}

/**
 * Layout products to grid - convert from 1-dimension to 2-dimensions
 *
 * @param products Array of products
 * @param productsPerRow How many products should be per row
 *
 * @return Product grid
 */
const buildGrid = (products: ProductMo[], productsPerRow: number): ProductMo[][] => {
    const result: ProductMo[][] = []
    let row: ProductMo[] = []

    products.forEach(product => {
        row.push(product)
        if (row.length === productsPerRow) {
            // Filled row - add it and create a new one
            result.push(row)
            row = []
        }
    })

    if (row.length > 0) {
        result.push(row)
    }

    return result
}

/**
 * Adapter for working with products
 */
export const ProductsAdapter = {
    /**
     * Loads products from API, sanitizes them, maps to our objects and sort according to our needs.
     * When all is done, result is set to state
     *
     * @param dispatch Dispatch used to set loading/error/data into state
     */
    loadProducts: async (dispatch: LoadedDataDispatch<ProductsDataMo>) => {
        dispatch(LoadedDataActions.startLoading())

        // Load data
        const response = await HttpService.post('products', findRequest)

        // TODO - check HTTP code? - TODO

        if (!response) {
            // Network error
            dispatch(LoadedDataActions.loadError())
            return
        }

        const apiResponse: ProductResponseApi = await response.json()

        if (apiResponse.err) {
            // API error
            LoggingService.logError('Load data error from API', { apiMessage: apiResponse.msg })
            dispatch(LoadedDataActions.loadError())
            return
        }

        // Map products
        const { products, numErrors } = ProductsMapper.mapProductsFromApi(apiResponse.data || [])
        if (numErrors > 0) {
            LoggingService.logWarning(`${numErrors} products could not be mapped due to errors`)
        }

        // Sort products according to various criteria
        const topProductsGrid = buildGrid(products, PRODUCTS_PER_ROW)
        const topSellingProducts = [...products].sort(topSellingComparator)
        const topSellingProductsGrid = buildGrid([...products].sort(topSellingComparator), PRODUCTS_PER_ROW)
        const lowestPriceFirstProductsGrid = buildGrid([...products].sort(lowestPriceComparator), PRODUCTS_PER_ROW)
        const highestPriceFirstProductsGrid = buildGrid([...products].sort(highestPriceComparator), PRODUCTS_PER_ROW)

        // Create final object & dispatch data
        const data = {
            topSellingProducts,
            topProductsGrid,
            topSellingProductsGrid,
            lowestPriceFirstProductsGrid,
            highestPriceFirstProductsGrid,
        } as ProductsDataMo

        dispatch(LoadedDataActions.dataLoaded(data))
    },
}
